<?php
require_once __DIR__ . '/../MefworksUnitTest.php';

class EntryTest extends MefworksTestCase
{
	public function setUp()
	{
		parent::setUp();
	}

	public function testGetters()
	{
		$dt = new DateTime;
		$level = Psr\Log\LogLevel::DEBUG;
		$message = 'Hello, World';
		$context = ['foo' => 'bar'];

		$entry = new mef\Log\Entry\Entry($dt, $level, $message, $context);

		$this->assertEquals($dt, $entry->getDateTime());
		$this->assertEquals($level, $entry->getLevel());
		$this->assertEquals($message, $entry->getMessage());
		$this->assertEquals($context, $entry->getContext());
	}
}