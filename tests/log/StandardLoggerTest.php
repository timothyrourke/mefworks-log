<?php
require_once __DIR__ . '/../MefworksUnitTest.php';

class StandardLoggerTest extends MefworksTestCase
{
	public function setUp()
	{
		parent::setUp();
	}

	public function testConstructor()
	{
		$logger = new mef\Log\StandardLogger;

		$this->assertTrue($logger->getHandler() instanceof mef\Log\Handler\ChainHandler);
	}
}