<?php
require_once __DIR__ . '/../../MefworksUnitTest.php';

use Psr\Log\LogLevel;

class MemoryProcessorTest extends MefworksTestCase
{
	public function testBasic()
	{
		$memoryProcessor = new \mef\Log\Processor\MemoryProcessor;

		$entry = new mef\Log\Entry\MutableEntry(
			new DateTimeImmutable,
			LogLevel::INFO,
			'Hello, World!',
			[]
		);

		$memoryProcessor->process($entry);

		$this->assertTrue(isset($entry->getContext()['memory_usage']));
	}
}