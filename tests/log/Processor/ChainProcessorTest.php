<?php
require_once __DIR__ . '/../../MefworksUnitTest.php';

use Psr\Log\LogLevel;

class ChainProcessorTest extends MefworksTestCase
{
	public function testBasic()
	{
		$chainProcessor = new \mef\Log\Processor\ChainProcessor;
		$chainProcessor->addProcessor(new \mef\Log\Processor\NullProcessor);

		$entry = new mef\Log\Entry\MutableEntry(
			new DateTimeImmutable,
			LogLevel::INFO,
			'Hello, World!',
			[]
		);

		$chainProcessor->process($entry);
	}
}