<?php
require_once __DIR__ . '/../MefworksUnitTest.php';

class LoggerTest extends MefworksTestCase
{
	public function setUp()
	{
		parent::setUp();

		$factory = new mef\Log\Entry\DeferredEntryFactory;
		$handler = new mef\Log\Handler\MemoryHandler;

		$this->logger = new mef\Log\Logger($factory, $handler);
	}

	public function testConvenienceFunctions()
	{
		$this->logger->debug("debug");
		$this->logger->info("info");
		$this->logger->notice("notice");
		$this->logger->warning("warning");
		$this->logger->error("error");
		$this->logger->critical("critical");
		$this->logger->alert("alert");
		$this->logger->emergency("emergency");

		$entries = $this->logger->getHandler()->getEntries();

		$this->assertEquals(8, count($entries));
		$this->assertEquals(Psr\Log\LogLevel::DEBUG, $entries[0]->getLevel());
		$this->assertEquals(Psr\Log\LogLevel::INFO, $entries[1]->getLevel());
		$this->assertEquals(Psr\Log\LogLevel::NOTICE, $entries[2]->getLevel());
		$this->assertEquals(Psr\Log\LogLevel::WARNING, $entries[3]->getLevel());
		$this->assertEquals(Psr\Log\LogLevel::ERROR, $entries[4]->getLevel());
		$this->assertEquals(Psr\Log\LogLevel::CRITICAL, $entries[5]->getLevel());
		$this->assertEquals(Psr\Log\LogLevel::ALERT, $entries[6]->getLevel());
		$this->assertEquals(Psr\Log\LogLevel::EMERGENCY, $entries[7]->getLevel());
	}

	/**
	 * @expectedException Psr\Log\InvalidArgumentException
	 */
	public function testInvalidLogLevel()
	{
		$this->logger->log("invalid-level", "Test");
	}

	public function testHandlerSetter()
	{
		$handler = new mef\Log\Handler\MemoryHandler;

		$this->logger->setHandler($handler);

		$this->assertSame($handler, $this->logger->getHandler());
	}

	public function testFactoryGetterSetter()
	{
		$this->assertTrue($this->logger->getLogEntryFactory() instanceof mef\Log\Entry\EntryFactoryInterface);

		$factory = new mef\Log\Entry\DeferredEntryFactory;
		$this->logger->setLogEntryFactory($factory);

		$this->assertSame($factory, $this->logger->getLogEntryFactory());
	}
}