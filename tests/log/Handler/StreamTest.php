<?php namespace mef\Log\Handler;

require_once __DIR__ . '/../../MefworksUnitTest.php';

use Psr\Log\LogLevel;

class StreamTest extends \MefworksTestCase
{
	public function testConstructor()
	{
		$fp = fopen('php://memory', 'w');

		$formatter = new \mef\Log\Formatter\NullFormatter;

		$streamHandler = new \mef\Log\Handler\StreamHandler($fp);
		$this->assertNull($streamHandler->getLogEntryFormatter());

		$streamHandler = new \mef\Log\Handler\StreamHandler($fp, $formatter);
		$this->assertEquals($formatter, $streamHandler->getLogEntryFormatter());

		fclose($fp);
	}

	public function testLog()
	{
		$fp = fopen('php://memory', 'w+');

		$formatter = new \mef\Log\Formatter\CallbackFormatter(function(\mef\Log\Entry\EntryInterface $entry) {
			return $entry->getMessage();
		});

		$streamHandler = new \mef\Log\Handler\StreamHandler($fp, $formatter);

		$logger = new \mef\Log\StandardLogger($streamHandler);
		$logger->info('Hello, World!');

		fseek($fp, 0, SEEK_SET);

		$this->assertEquals('Hello, World!' . PHP_EOL, fgets($fp));

		fclose($fp);
	}

	public function testFilter()
	{
		$fp = fopen('php://memory', 'w+');

		$streamHandler = new \mef\Log\Handler\StreamHandler($fp);
		$streamHandler->filter(LogLevel::WARNING);
		$streamHandler->handleLogEntry(new \mef\Log\Entry\Entry(new \DateTimeImmutable, LogLevel::WARNING, 'Filter Me'));

		fseek($fp, 0, SEEK_SET);
		$this->assertSame(false, fgets($fp));
		fclose($fp);
	}
}