<?php namespace mef\Log\Handler;

require_once __DIR__ . '/../../MefworksUnitTest.php';

use Psr\Log\LogLevel;

class StdoutTest extends \MefworksTestCase
{
	public function testStdout()
	{
		$outputHandler = new \mef\Log\Handler\StdoutHandler;

		$this->assertTrue($outputHandler instanceof \mef\Log\Handler\StreamHandler);
	}
}