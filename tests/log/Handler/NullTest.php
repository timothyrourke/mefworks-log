<?php namespace mef\Log\Handler;

require_once __DIR__ . '/../../MefworksUnitTest.php';

class NullTest extends \MefworksTestCase
{
	public function testLevels()
	{
		$nullHandler = new \mef\Log\Handler\NullHandler;
		$entry = new \mef\Log\Entry\Entry(new \DateTime, \Psr\Log\LogLevel::WARNING, 'Hello');
		$nullHandler->handleLogEntry($entry);
	}
}