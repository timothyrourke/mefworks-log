<?php namespace mef\Log\Handler;

require_once __DIR__ . '/../../MefworksUnitTest.php';

class StderrTest extends \MefworksTestCase
{
	public function testStdout()
	{
		$outputHandler = new \mef\Log\Handler\StderrHandler;

		$this->assertTrue($outputHandler instanceof \mef\Log\Handler\StreamHandler);
	}
}