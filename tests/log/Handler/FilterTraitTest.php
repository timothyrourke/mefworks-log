<?php
require_once __DIR__ . '/../../MefworksUnitTest.php';

use Psr\Log\LogLevel;

class FilterTraitTest extends MefworksTestCase
{
	public function testFilter()
	{
		$filter = new MockFilter;

		$this->assertTrue($filter->willHandleLevel(LogLevel::INFO));

		$filter->filter(LogLevel::INFO);

		$this->assertFalse($filter->willHandleLevel(LogLevel::INFO));
	}

	public function testFilterAll()
	{
		$filter = new MockFilter;

		$filter->filterAll();

		$this->assertFalse($filter->willHandleLevel(LogLevel::DEBUG));
		$this->assertFalse($filter->willHandleLevel(LogLevel::INFO));
		$this->assertFalse($filter->willHandleLevel(LogLevel::NOTICE));
		$this->assertFalse($filter->willHandleLevel(LogLevel::WARNING));
		$this->assertFalse($filter->willHandleLevel(LogLevel::ERROR));
		$this->assertFalse($filter->willHandleLevel(LogLevel::CRITICAL));
		$this->assertFalse($filter->willHandleLevel(LogLevel::ALERT));
		$this->assertFalse($filter->willHandleLevel(LogLevel::EMERGENCY));
	}

	public function testFilterAllExcept()
	{
		$filter = new MockFilter;

		$filter->filterAllExcept(LogLevel::ERROR);

		$this->assertFalse($filter->willHandleLevel(LogLevel::DEBUG));
		$this->assertFalse($filter->willHandleLevel(LogLevel::INFO));
		$this->assertFalse($filter->willHandleLevel(LogLevel::NOTICE));
		$this->assertFalse($filter->willHandleLevel(LogLevel::WARNING));
		$this->assertTrue($filter->willHandleLevel(LogLevel::ERROR));
		$this->assertFalse($filter->willHandleLevel(LogLevel::CRITICAL));
		$this->assertFalse($filter->willHandleLevel(LogLevel::ALERT));
		$this->assertFalse($filter->willHandleLevel(LogLevel::EMERGENCY));
	}

	public function testFilterLowerThan()
	{
		$filter = new MockFilter;
		$filter->filterLowerThan(LogLevel::WARNING);

		$this->assertFalse($filter->willHandleLevel(LogLevel::INFO));
		$this->assertTrue($filter->willHandleLevel(LogLevel::WARNING));
		$this->assertTrue($filter->willHandleLevel(LogLevel::EMERGENCY));
	}

	public function testFilterHigherThan()
	{
		$filter = new MockFilter;
		$filter->filterHigherThan(LogLevel::WARNING);

		$this->assertTrue($filter->willHandleLevel(LogLevel::INFO));
		$this->assertTrue($filter->willHandleLevel(LogLevel::WARNING));
		$this->assertFalse($filter->willHandleLevel(LogLevel::EMERGENCY));
	}

	public function testUnfilter()
	{
		$filter = new MockFilter;
		$filter->filter(LogLevel::INFO);
		$filter->unfilter(LogLevel::INFO);

		$this->assertTrue($filter->willHandleLevel(LogLevel::INFO));
	}

	public function testUnfilterLowerThan()
	{
		$filter = new MockFilter;
		$filter->filter(LogLevel::INFO);
		$filter->filter(LogLevel::WARNING);
		$filter->unfilterLowerThan(LogLevel::WARNING);

		$this->assertTrue($filter->willHandleLevel(LogLevel::INFO));
		$this->assertFalse($filter->willHandleLevel(LogLevel::WARNING));
	}

	public function testUnfilterHigherThan()
	{
		$filter = new MockFilter;
		$filter->filter(LogLevel::INFO);
		$filter->filter(LogLevel::WARNING);
		$filter->unfilterHigherThan(LogLevel::INFO);

		$this->assertFalse($filter->willHandleLevel(LogLevel::INFO));
		$this->assertTrue($filter->willHandleLevel(LogLevel::WARNING));
	}
}

class MockFilter
{
	use mef\Log\FilterTrait;
}