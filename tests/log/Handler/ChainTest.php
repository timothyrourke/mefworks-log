<?php
require_once __DIR__ . '/../../MefworksUnitTest.php';

use Psr\Log\LogLevel;

class ChainTest extends MefworksTestCase
{
	public function testBasic()
	{
		$memoryHandler = new mef\Log\Handler\MemoryHandler;

		$chainHandler = new mef\Log\Handler\ChainHandler;
		$chainHandler->pushHandler($memoryHandler);

		$logger = new mef\Log\StandardLogger($chainHandler);

		$logger->info('a');
		$this->assertEquals(1, count($memoryHandler));

		$logger->info('b');
		$this->assertEquals(2, count($memoryHandler));
	}

	public function testHandleLevelPassThrough()
	{
		$memoryHandler = new mef\Log\Handler\MemoryHandler;
		$memoryHandler->filterLowerThan(LogLevel::WARNING);

		$chainHandler = new mef\Log\Handler\ChainHandler;
		$chainHandler->pushHandler($memoryHandler);

		$logger = new mef\Log\StandardLogger($chainHandler);

		$logger->info('a');
		$this->assertEquals(0, count($memoryHandler));

		$logger->warning('b');
		$this->assertEquals(1, count($memoryHandler));
	}

	public function testPushPopHandler()
	{
		$nullHandler = new mef\Log\Handler\NullHandler;
		$chainHandler = new mef\Log\Handler\ChainHandler;

		$chainHandler->pushHandler($nullHandler);
		$this->assertSame($nullHandler, $chainHandler->popHandler());
	}

	/**
	 * @expectedException Exception
	 */
	public function testPopEmpty()
	{
		$chainHandler = new mef\Log\Handler\ChainHandler;
		$chainHandler->popHandler();
	}

	public function testGetHandlers()
	{
		$nullHandler = new mef\Log\Handler\NullHandler;
		$chainHandler = new mef\Log\Handler\ChainHandler;

		$chainHandler->pushHandler($nullHandler);
		$chainHandler->pushHandler($nullHandler);

		$this->assertSame(
			[$nullHandler, $nullHandler],
			$chainHandler->getHandlers()
		);
	}

	public function testIteration()
	{
		$nullHandler = new mef\Log\Handler\NullHandler;
		$chainHandler = new mef\Log\Handler\ChainHandler;

		$chainHandler->pushHandler($nullHandler);
		$chainHandler->pushHandler($nullHandler);

		$this->assertSame(
			iterator_to_array($chainHandler),
			$chainHandler->getHandlers()
		);
	}

	public function testFilter()
	{
		$chainHandler = new mef\Log\Handler\ChainHandler;
		$chainHandler->filter(LogLevel::WARNING);
		$chainHandler->handleLogEntry(new \mef\Log\Entry\Entry(new DateTimeImmutable, LogLevel::WARNING, 'Filter Me'));
	}

	public function testConsume()
	{
		$memoryHandler = new mef\Log\Handler\MemoryHandler;
		$chainHandler = new mef\Log\Handler\ChainHandler;

		$chainHandler->pushHandler($memoryHandler, true);
		$chainHandler->pushHandler($memoryHandler);

		$chainHandler->handleLogEntry(new \mef\Log\Entry\Entry(new DateTimeImmutable, LogLevel::WARNING, 'Remember Me'));

		$this->assertSame(1, count($memoryHandler));
	}

	public function testNotConsumed()
	{
		$chainHandler = new mef\Log\Handler\ChainHandler;
		$nullHandler = new mef\Log\Handler\NullHandler;

		$chainHandler->pushHandler($nullHandler);
		$this->assertFalse($chainHandler->handleLogEntry(new \mef\Log\Entry\Entry(new DateTimeImmutable, LogLevel::WARNING, 'Ignore Me')));
	}

	public function testNotConsumedBecauseEmpty()
	{
		$chainHandler = new mef\Log\Handler\ChainHandler;

		$this->assertFalse($chainHandler->handleLogEntry(new \mef\Log\Entry\Entry(new DateTimeImmutable, LogLevel::WARNING, 'Ignore Me')));
	}
}