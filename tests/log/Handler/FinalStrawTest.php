<?php
require_once __DIR__ . '/../../MefworksUnitTest.php';

use Psr\Log\LogLevel;

class FinalStrawTest extends MefworksTestCase
{
	public function setUp()
	{
		parent::setUp();
	}

	public function testConstructor()
	{
		$nullHandler = new mef\Log\Handler\NullHandler;
		$finalStrawHandler = new mef\Log\Handler\FinalStrawHandler($nullHandler);
	}

	/**
	 * @expectedException Psr\Log\InvalidArgumentException
	 */
	public function testInvalidLevel()
	{
		$nullHandler = new mef\Log\Handler\NullHandler;
		$finalStrawHandler = new mef\Log\Handler\FinalStrawHandler($nullHandler, 'INVALID-LEVEL');
	}

	public function testCounts()
	{
		$memoryHandler = new mef\Log\Handler\MemoryHandler;
		$finalStrawHandler = new mef\Log\Handler\FinalStrawHandler($memoryHandler, LogLevel::ERROR);

		$logger = new mef\Log\StandardLogger($finalStrawHandler);

		// First Pass
		$logger->info('He\'s wearing a red shirt.');
		$this->assertEquals(0, count($memoryHandler));

		$logger->critical('He\'s dead, Jim.');
		$this->assertEquals(2, count($memoryHandler));

		// Second Pass
		$logger->info('He\'s wearing a red shirt.');
		$this->assertEquals(3, count($memoryHandler));

		$logger->critical('He\'s dead, Jim.');
		$this->assertEquals(4, count($memoryHandler));
	}

	public function testFilter()
	{
		$memoryHandler = new mef\Log\Handler\MemoryHandler;
		$finalStrawHandler = new mef\Log\Handler\FinalStrawHandler($memoryHandler);
		$finalStrawHandler->filter(LogLevel::WARNING);
		$finalStrawHandler->handleLogEntry(new \mef\Log\Entry\Entry(new DateTimeImmutable, LogLevel::WARNING, 'Filter Me'));

		$this->assertSame(0, count($memoryHandler));
	}

	public function testDidNotConsumeBatch()
	{
		$nullHandler = new mef\Log\Handler\NullHandler;
		$finalStrawHandler = new mef\Log\Handler\FinalStrawHandler($nullHandler);
		$finalStrawHandler->handleLogEntry(new \mef\Log\Entry\Entry(new DateTimeImmutable, LogLevel::ERROR, 'Filter Me'));

		$this->assertSame(0, count($finalStrawHandler));
	}
}