<?php
require_once __DIR__ . '/../../MefworksUnitTest.php';

use Psr\Log\LogLevel;

class FileHandlerTest extends MefworksTestCase
{
	public function testBasic()
	{
		$fileHandler = new mef\Log\Handler\FileHandler('php://memory');

		$logger = new mef\Log\StandardLogger($fileHandler);

		$logger->info('a');
	}
}