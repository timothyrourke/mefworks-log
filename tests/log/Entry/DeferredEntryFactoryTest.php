<?php
require_once __DIR__ . '/../../MefworksUnitTest.php';

use mef\Log\Entry\DeferredEntryFactory;

class DeferredEntryFactoryTest extends MefworksTestCase
{
	public function setUp()
	{
		parent::setUp();
	}

	public function testNoInterpolation()
	{
		$factory = new DeferredEntryFactory;

		$this->assertNull($factory->getStringInterpolator());
	}

	public function testInterpolationSetter()
	{
		$interpolator = new \mef\StringInterpolation\PlaceholderInterpolator(new mef\Stringifier\Stringifier);
		$factory = new DeferredEntryFactory;
		$factory->setStringInterpolator($interpolator);

		$this->assertSame($interpolator, $factory->getStringInterpolator());

		$factory->unsetStringInterpolator();
		$this->assertNull($factory->getStringInterpolator());
	}

	public function testProcessor()
	{
		$factory = new DeferredEntryFactory;

		$processor = new mef\Log\Processor\NullProcessor;

		$factory->setProcessor($processor);
		$this->assertEquals($processor, $factory->getProcessor());

		$factory->unsetProcessor();
		$this->assertNull($factory->getProcessor());
	}

	public function testDefaultTimeZone()
	{
		$factory = new DeferredEntryFactory;

		$this->assertSame('UTC', $factory->getTimeZone()->getName());
	}

	public function testUpdateTimeZone()
	{
		$factory = new DeferredEntryFactory;

		$tz = new DateTimeZone('America/Chicago');

		$factory->setTimeZone($tz);

		$this->assertSame($tz, $factory->getTimeZone());
	}
}