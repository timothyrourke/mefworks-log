<?php
require_once __DIR__ . '/../../MefworksUnitTest.php';

use Psr\Log\LogLevel;

use mef\Log\Entry\MutableEntry;

/**
 * @coversDefaultClass \mef\Log\Entry\MutableEntry
 */
class MutableEntryTest extends MefworksTestCase
{
	/**
	 * @covers ::setMessage
	 */
	public function testSetMessage()
	{
		$entry = new MutableEntry(new DateTimeImmutable, LogLevel::ERROR, 'Hello');

		$message = 'Goodbye!';
		$entry->setMessage($message);
		$this->assertSame($message, $entry->getMessage());
	}

	/**
	 * @covers ::updateContext
	 */
	public function testUpdateContext()
	{
		$entry = new MutableEntry(new DateTimeImmutable, LogLevel::ERROR, 'Hello');

		$key = 'key';
		$value = 'Goodbye!';

		$entry->updateContext($key, $value);

		$this->assertSame($value, $entry->getContext()[$key]);
	}
}