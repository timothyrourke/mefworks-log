<?php
require_once __DIR__ . '/../../MefworksUnitTest.php';

use Psr\Log\LogLevel;
use mef\Log\Entry\DeferredEntry;
use mef\Log\Processor\NullProcessor;

use mef\Stringifier\Stringifier;
use mef\StringInterpolation\PlaceholderInterpolator;

class DeferredEntryTest extends MefworksTestCase
{
	public function setUp()
	{
		parent::setUp();
	}

	public function testSimpleProperties()
	{
		$dt = new DateTimeImmutable;
		$level = LogLevel::WARNING;
		$message = 'Message';
		$append = 'Append';

		$entry = new DeferredEntry($dt, $level, $message);

		$this->assertEquals($dt, $entry->getDateTime());
		$this->assertSame($level, $entry->getLevel());

		$entry->setMessage($entry->getMessage() . $append);
		$this->assertSame($message . $append, $entry->getMessage());
	}

	public function testContext()
	{
		$dt = new DateTimeImmutable;
		$level = LogLevel::WARNING;
		$message = 'Message';
		$context = ['foo' => 'bar'];

		$entry = new DeferredEntry($dt, $level, $message, $context);

		$this->assertEquals($context, $entry->getContext());

		$entry->updateContext('foo', null);
		$this->assertEquals(null, $entry->getContext()['foo']);
	}

	public function testProcessor()
	{
		$dt = new DateTimeImmutable;
		$level = LogLevel::WARNING;
		$message = 'Message';
		$context = ['foo' => 'bar'];

		$entry = new DeferredEntry($dt, $level, $message, $context, new NullProcessor);

		$this->assertEquals($context, $entry->getContext());
	}

	public function testInterpolator()
	{
		$dt = new DateTimeImmutable;
		$level = LogLevel::WARNING;
		$message = 'Message';
		$context = ['foo' => 'bar'];

		$entry = new DeferredEntry(
			$dt, $level, $message, $context, null,
			new PlaceholderInterpolator(new Stringifier)
		);

		$this->assertEquals($message, $entry->getMessage());
	}

	public function testToString()
	{
		$dt = new DateTimeImmutable;
		$level = LogLevel::WARNING;
		$message = 'Message';
		$context = ['foo' => 'bar'];

		$entry = new DeferredEntry(
			$dt, $level, $message, $context, null,
			new PlaceholderInterpolator(new Stringifier)
		);

		$this->assertTrue(is_string((string) $entry));
	}
}