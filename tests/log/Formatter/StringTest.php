<?php
require_once __DIR__ . '/../../MefworksUnitTest.php';

use mef\Log\Formatter\StringFormatter;
use mef\StringInterpolation\PlaceholderInterpolator;
use mef\Stringifier\Stringifier;

class StringTest extends MefworksTestCase
{
	public function testString()
	{
		$formatter = new StringFormatter(
			new PlaceholderInterpolator(new Stringifier),
			'{date} [{level}] {message} {context}'
		);
		$memoryHandler = new mef\Log\Handler\MemoryHandler;

		$logger = new mef\Log\StandardLogger($memoryHandler);
		$logger->info('abcdef', ['foo' => 'bar']);

		$logEntry = $memoryHandler->dequeue();
		$output = $formatter->formatLogEntry($logEntry);

		$this->assertTrue(is_string($output));
	}
}