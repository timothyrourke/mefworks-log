<?php namespace mef\Log\Handler;

use Traversable;

use mef\Log\Entry\EntryInterface;
use mef\Log\Formatter\FormatterAwareTrait;
use mef\Log\Formatter\FormatterInterface;
use mef\Log\Formatter\StringFormatter;

/**
 * Log to any PHP stream.
 */
class StreamHandler extends AbstractHandler
{
	use FormatterAwareTrait;

	/**
	 * A stream handle.
	 *
	 * @var resource
	 */
	private $stream;

	/**
	 * Constructor.
	 *
	 * If no $formatter is supplied, the entry's __toString is used.
	 *
	 * @param resource $stream   A handle to a stream or file.
	 * @param \mef\Log\Formatter\FormatterInterface $formatter
	 */
	public function __construct($stream, FormatterInterface $formatter = null)
	{
		$this->stream = $stream;
		$this->logEntryFormatter = $formatter;
	}

	/**
	 * Write the formatted entry to the stream.
	 *
	 * @param  mef\Log\EntryInterface $entry
	 *
	 * @return boolean  true if consumed
	 */
	public function handleLogEntry(EntryInterface $entry)
	{
		if ($this->willHandleLevel($entry->getLevel()) === false)
		{
			return false;
		}

		if ($this->logEntryFormatter !== null)
		{
			fwrite($this->stream, $this->logEntryFormatter->formatLogEntry($entry) . PHP_EOL);
		}
		else
		{
			fwrite($this->stream, ((string) $entry) . PHP_EOL);
		}

		return true;
	}
}