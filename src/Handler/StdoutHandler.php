<?php namespace mef\Log\Handler;

use mef\Log\Formatter\FormatterInterface;

/**
 * Log to stdout.
 *
 * This is NOT equivalent to logging via 'echo'. i.e. It will not be seen
 * as part of the browser's output if running from a web server. (It may show
 * up in the web server's log or on the shell.)
 *
 * If you want to log to PHP's output, use mef\Log\Handler\Output.
 */
class StdoutHandler extends AbstractStdHandler
{
	/**
	 * Constructor
	 *
	 * Uses the predefined STDOUT file handler if available; otherwise opens
	 * the 'php://stdout' file.
	 *
	 * @param \mef\Log\Formatter\FormatterInterface $formatter
	 */
	public function __construct(FormatterInterface $formatter = null)
	{
		parent::__construct('STDOUT', 'php://stdout', $formatter);
	}
}