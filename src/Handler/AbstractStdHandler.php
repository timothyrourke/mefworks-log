<?php namespace mef\Log\Handler;

use mef\Log\Formatter\FormatterInterface;

/**
 * Log to a standard handler.
 */
abstract class AbstractStdHandler extends StreamHandler
{
	/**
	 * Constructor
	 *
	 * Uses the predefined file handler if available; otherwise open
	 * the given path
	 *
	 * @param string $constant  The name of the std constant (e.g., 'STDOUT')
	 * @param string $path      The path to open if the constant is not defined
	 * @param \mef\Log\Formatter\FormatterInterface $formatter
	 */
	public function __construct($constant, $path, FormatterInterface $formatter = null)
	{
		if (defined($constant))
		{
			parent::__construct(constant($constant), $formatter);
		}
		else
		{
			// @codeCoverageIgnoreStart
			// This can never be reached when doing unit tests via CLI
			parent::__construct(fopen($path, 'w'), $formatter);
			// @codeCoverageIgnoreEnd
		}
	}
}