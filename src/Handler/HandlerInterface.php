<?php namespace mef\Log\Handler;

use Traversable;

use mef\Log\Entry\EntryInterface;

/**
 * Objects that implement HandlerInterface are supposed to store the
 * LogEntryInterface object in a meaningful way. e.g. A handler may save the
 * log entry to a database or text file.
 */
interface HandlerInterface
{
	/**
	 * Do something with the $entry. e.g. Save it to disk.
	 *
	 * @param  EntryInterface $entry
	 *
	 * @return boolean  true if consumed
	 */
	public function handleLogEntry(EntryInterface $entry);

	/**
	 * Do something with the $entry. e.g. Save it to disk.
	 *
	 * @param  EntryInterface $entry
	 */
	public function handleLogEntryBatch(Traversable $entry);
}