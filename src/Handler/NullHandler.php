<?php namespace mef\Log\Handler;

use Traversable;

use mef\Log\Entry\EntryInterface;

class NullHandler implements HandlerInterface
{
	/**
	 * Do nothing.
	 *
	 * @param \mef\Log\EntryInterface $entry
	 *
	 * @return boolean  Always returns false
	 */
	public function handleLogEntry(EntryInterface $entry)
	{
		return false;
	}

	/**
	 * Do nothing.
	 *
	 * @param  Traversable $entries
	 */
	public function handleLogEntryBatch(Traversable $entries)
	{
	}
}