<?php namespace mef\Log\Handler;

use Countable;
use Exception;
use SplQueue;

use Psr\Log\InvalidArgumentException;
use Psr\Log\LogLevel;

use mef\Log\Entry\EntryInterface;
use mef\Log\Handler\HandlerAwareInterface;
use mef\Log\Handler\HandlerAwareTrait;
use mef\Log\Logger;

/**
 * Holds on to log entries until some minimum level is met.
 *
 * Once that minimum level is met, all queued log entries are passed to the
 * inner handler to be processed as normal.
 *
 * The process repeats itself. That is, future log entries that don't meet the
 * minimum level will again be queued.
 *
 * Events that were filtered out via the FilterTrait functionality will not be
 * queued and will not trigger the minimum level threshold. Generally it is
 * not very useful to attach filters to this particular handler.
 */
class FinalStrawHandler extends AbstractHandler implements HandlerAwareInterface, Countable
{
	use HandlerAwareTrait;

	/**
	 * All queued entries.
	 *
	 * Note that iterating over the entries delete them.
	 *
	 * @var SplQueue
	 */
	private $entries;

	/**
	 * The minimum level at which all queued entries will be logged.
	 *
	 * This is a Psr\Log\LogLevel constant.
	 *
	 * @var string
	 */
	private $minimumLevel;

	/**
	 * If true, queue all entries after the final straw is reached.
	 *
	 * @var boolean
	 */
	private $requeue;

	/**
	 * If true, buffer the event (if lower than minimum level)
	 *
	 * @var boolean
	 */
	private $isBuffering = true;

	/**
	 * Constructor
	 *
	 * @param HandlerInterface  $handler       the handler to process logs
	 * @param string            $minimumLevel  a Psr\Log\LogLevel constnat
	 * @param boolean           $requeue       whether or not entries following
	 *                                           the "final straw" should be
	 *                                           queued regardless of their
	 *                                           level
	 */
	public function __construct(HandlerInterface $handler, $minimumLevel = LogLevel::ERROR, $requeue = false)
	{
		$this->clear();

		$this->handler = $handler;

		$levels = Logger::getErrorLevels();

		if (!isset($levels[$minimumLevel]))
		{
			throw new InvalidArgumentException($minimumLevel);
		}

		$this->minimumLevel = $levels[$minimumLevel];

		$this->requeue = (bool) $requeue;
	}

	/**
	 * Queues the log entry. If the entry is at or higher than the minimum
	 * level, then it and all prior logs are passed to the inner handler.
	 *
	 * @param  mef\Log\EntryInterface $entry
	 *
	 * @param boolean  true if consumed
	 */
	public function handleLogEntry(EntryInterface $entry)
	{
		if ($this->willHandleLevel($entry->getLevel()) === false)
		{
			return false;
		}

		if ($this->isBuffering === false)
		{
			$this->handler->handleLogEntry($entry);
		}
		else
		{
			$this->entries->enqueue($entry);
			$levels = Logger::getErrorLevels();

			if ($levels[$entry->getLevel()] >= $this->minimumLevel)
			{
				$this->flush();
			}
		}

		return true;
	}

	/**
	 * Clear the queued entries.
	 */
	public function clear()
	{
		$this->entries = new SplQueue;
		$this->entries->setIteratorMode(SplQueue::IT_MODE_DELETE);
	}

	/**
	 * Force queued entries to be flushed to the underlying handler.
	 */
	public function flush()
	{
		$this->handler->handleLogEntryBatch($this->entries);

		if ($this->requeue === false)
		{
			$this->isBuffering = false;
		}

		if (!$this->entries->isEmpty())
		{
			$this->clear();
		}
	}

	/**
	 * Return the number of queued entries.
	 *
	 * @return integer
	 */
	public function count()
	{
		return count($this->entries);
	}
}