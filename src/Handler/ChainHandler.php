<?php namespace mef\Log\Handler;

use ArrayIterator;
use Exception;
use IteratorAggregate;

use mef\Log\Entry\EntryInterface;

/**
 * Chain many handlers together so that the same log entry can be processed
 * by multiple handlers.
 */
class ChainHandler extends AbstractHandler implements IteratorAggregate
{
	/**
	 * An array of attached handlers.
	 *
	 * @var array
	 */
	private $handlers = [];

	/**
	 * Add a handler to the end of the list.
	 *
	 * @param \mef\Log\Handler\HandlerInterface $handler
	 * @param boolean $stopOnConsume  if set to true, the chain will stop
	 *                                  if the handler consumes the event.
	 */
	public function pushHandler(HandlerInterface $handler, $stopOnConsume = false)
	{
		$this->handlers[] = new ChainHandlerInfo($handler, $stopOnConsume);
	}

	/**
	 * Remove the last handler from the list.
	 *
	 * @return \mef\Log\Handler\HandlerInterface  the handler that was removed
	 */
	public function popHandler()
	{
		if (count($this->handlers) === 0)
		{
			throw new Exception('There are no handlers to pop');
		}

		return array_pop($this->handlers)->handler;
	}

	/**
	 * Return all attached handlers.
	 *
	 * @return array  An array of mef\Log\Handler\HandlerInterface objects
	 */
	public function getHandlers()
	{
		return array_map(function($info) { return $info->handler; }, $this->handlers);
	}

	/**
	 * Return an iterator for the handlers.
	 *
	 * @return Traversable
	 */
	public function getIterator()
	{
		foreach ($this->handlers as $handlerInfo)
		{
			yield $handlerInfo->handler;
		}
	}

	/**
	 * Handle the log entry by iterating over all attached handlers.
	 *
	 * @param  mef\Log\EntryInterface $entry
	 *
	 * @return boolean  true if consumed
	 */
	public function handleLogEntry(EntryInterface $entry)
	{
		$consumed = false;

		if ($this->willHandleLevel($entry->getLevel()) === true)
		{
			foreach ($this->handlers as $handlerInfo)
			{
				if ($handlerInfo->handler->handleLogEntry($entry) === true)
				{
					$consumed = true;

					if ($handlerInfo->stopOnConsume === true)
					{
						break;
					}
				}
			}
		}

		return $consumed;
	}
}