<?php namespace mef\Log\Entry;

/**
 * An interface that represents a log item.
 *
 * mef\Log\Entry implements this interface.
 */
interface EntryInterface
{
	/**
	 * Return the time of when the log entry was created.
	 *
	 * @return \DateTimeInterface
	 */
	public function getDateTime();

	/**
	 * Return the severity level.
	 *
	 * @return string
	 */
	public function getLevel();

	/**
	 * Return the message.
	 *
	 * @return string
	 */
	public function getMessage();

	/**
	 * Return the extra context data.
	 *
	 * @return array
	 */
	public function getContext();

	/**
	 * Return a string representation.
	 *
	 * @return string
	 */
	public function __toString();
}