<?php namespace mef\Log\Entry;

use DateTimeImmutable;
use DateTimeZone;

use mef\Log\Processor\ProcessorAwareInterface;
use mef\Log\Processor\ProcessorAwareTrait;
use mef\Log\Processor\ProcessorInterface;

use mef\StringInterpolation\StringInterpolatorAwareInterface;
use mef\StringInterpolation\StringInterpolatorAwareTrait;
use mef\StringInterpolation\StringInterpolatorInterface;

/**
 * Build a DeferredEntry object.
 */
class DeferredEntryFactory implements EntryFactoryInterface,
	ProcessorAwareInterface, StringInterpolatorAwareInterface
{
	use ProcessorAwareTrait;
	use StringInterpolatorAwareTrait;

	protected $timeZone;

	/**
	 * Contructor.
	 *
	 * @param \mef\StringInterpolation\StringInterpolatorInterface $interpolator
	 * @param \mef\Log\Processor\ProcessorInterface $processor
	 */
	public function __construct(
		StringInterpolatorInterface $interpolator = null,
		ProcessorInterface $processor = null,
		DateTimeZone $timeZone = null
	) {
		$this->stringInterpolator = $interpolator;
		$this->processor = $processor;
		$this->timeZone = $timeZone ?: new DateTimeZone('UTC');
	}

	/**
	 * Return the timezone for future log entries.
	 *
	 * @return \DateTimeZone
	 */
	public function getTimeZone()
	{
		return $this->timeZone;
	}

	/**
	 * Set the timezone for future log entries.
	 *
	 * The default timezone is UTC.
	 *
	 * @param \DateTimeZone $timeZone
	 */
	public function setTimeZone(DateTimeZone $timeZone)
	{
		$this->timeZone = $timeZone;
	}

	/**
	 * Unset the processor.
	 */
	public function unsetProcessor()
	{
		$this->processor = null;
	}

	/**
	 * Unset the string interpolator.
	 */
	public function unsetStringInterpolator()
	{
		$this->stringInterpolator = null;
	}

	/**
	 * Return a DeferredEntry object.
	 *
	 * Typically only called by the Logger.
	 *
	 * @param  string $level
	 * @param  string $message
	 * @param  array $context
	 * @return \mef\Log\Entry\DeferredEntry
	 */
	public function createLogEntry($level, $message, array $context = [])
	{
		return new DeferredEntry(
			new DateTimeImmutable('', $this->timeZone),
			$level,
			$message,
			$context,
			$this->processor,
			$this->stringInterpolator
		);
	}
}