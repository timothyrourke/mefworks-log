<?php namespace mef\Log\Entry;

use DateTimeInterface;

use mef\Log\Processor\ProcessorInterface;
use mef\StringInterpolation\StringInterpolatorInterface;

/**
 * A log entry that has its processing deferred until after one if its
 * properties is accessed.
 *
 * Normally there is no need to create one of these objects. It is the
 * responsibility of the Logger to create one and pass it to the Handler.
 *
 * The object is immutable.
 */
class DeferredEntry implements MutableEntryInterface
{
	use EntryToStringTrait;

	/**
	 * The time the log was created.
	 *
	 * @var \DateTimeInterface
	 */
	private $dateTime;

	/**
	 * The log level.
	 *
	 * It must be one of the Psr\Log\LogLevel constants.
	 *
	 * @var string
	 */
	private $level;

	/**
	 * The message to display.
	 *
	 * This message is in post-interpolated form (after processed() is called)
	 * i.e., The handler will (probably) log this message as-is.
	 *
	 * @var string
	 */
	private $message;

	/**
	 * Extra data that the handler may optionally use.
	 *
	 * @var array
	 */
	private $context;

	/**
	 * The processor (optional) that transforms the message and context.
	 *
	 * @var \mef\Log\Processor\ProcessorInterface
	 */
	private $processor;

	/**
	 * The interpolator that applies the context to the message.
	 *
	 * @var \mef\StringInterpolation\StringInterpolatorInterface
	 */
	private $interpolator;

	/**
	 * Set to true if the message has been processed.
	 *
	 * @var boolean
	 */
	private $isProcessed = false;

	/**
	 * Constructor.
	 *
	 * @param \DateTimeInterface $dateTime          time of log
	 * @param string             $level             a Psr\Log\LogLevel constant
	 * @param string             $message           the message to log
	 * @param array              $context           extra data
	 * @param \mef\Log\Processor\ProcessorInterface                $processor
	 * @param \mef\StringInterpolation\StringInterpolatorInterface $interpolator
	 */
	public function __construct(DateTimeInterface $dateTime, $level, $message,
		array $context = [], ProcessorInterface $processor = null,
		StringInterpolatorInterface $interpolator = null)
	{
		$this->dateTime = $dateTime;
		$this->level = (string) $level;
		$this->message = (string) $message;
		$this->context = $context;

		$this->processor = $processor;
		$this->interpolator = $interpolator;
	}

	/**
	 * Return the time of when the log entry was created.
	 *
	 * @return \DateTimeInterface
	 */
	public function getDateTime()
	{
		return $this->dateTime;
	}

	/**
	 * Return the severity level.
	 *
	 * @return string
	 */
	public function getLevel()
	{
		return $this->level;
	}

	/**
	 * Return the message.
	 *
	 * @return string
	 */
	public function getMessage()
	{
		if ($this->isProcessed === false)
		{
			$this->process();
		}

		return $this->message;
	}

	/**
	 * Set the message.
	 *
	 * @param string $message
	 */
	public function setMessage($message)
	{
		$this->message = (string) $message;
	}

	/**
	 * Return the extra context data.
	 *
	 * @return array
	 */
	public function getContext()
	{
		if ($this->isProcessed === false)
		{
			$this->process();
		}

		return $this->context;
	}

	/**
	 * Update the context for the given key.
	 *
	 * @param  string $key
	 * @param  mixed  $value
	 */
	public function updateContext($key, $value)
	{
		$this->context[$key] = $value;
	}

	/**
	 * Process and interpolate the original message and context as needed.
	 */
	private function process()
	{
		$this->isProcessed = true;

		if ($this->processor !== null)
		{
			$this->processor->process($this);
		}

		if ($this->interpolator !== null)
		{
			$interpolation = $this->interpolator->interpolate($this->message, $this->context);
			$this->message = $interpolation->getString();
			$this->context = array_diff_key($this->context, $interpolation->getUsedContext());
		}
	}
}