<?php namespace mef\Log\Entry;

trait EntryFactoryAwareTrait
{
	/**
	 * @var \mef\Log\Entry\EntryFactoryInterface
	 */
	protected $logEntryFactory;

	/**
	 * Return the log entry factory
	 *
	 * @param \mef\Log\Entry\EntryFactoryInterface
	 */
	public function getLogEntryFactory()
	{
		return $this->logEntryFactory;
	}

	/**
	 * Set the log entry factory.
	 *
	 * @param \mef\Log\Entry\EntryFactoryInterface
	 */
	public function setLogEntryFactory(EntryFactoryInterface $logEntryFactory)
	{
		$this->logEntryFactory = $logEntryFactory;
	}
}