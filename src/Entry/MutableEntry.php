<?php namespace mef\Log\Entry;

use \DateTimeInterface;

class MutableEntry extends Entry implements MutableEntryInterface
{
	/**
	 * Set the message.
	 *
	 * @param string $message
	 */
	public function setMessage($message)
	{
		$this->message = (string) $message;
	}

	/**
	 * Update the extra context data.
	 *
	 * @return array
	 */
	public function updateContext($key, $value)
	{
		$this->context[$key] = $value;
	}
}