<?php namespace mef\Log\Entry;

interface EntryFactoryInterface
{
	/**
	 * Return a LogEntryInterface object from the supplied parameters.
	 *
	 * @param  string $level
	 * @param  string $message
	 * @param  array $context
	 *
	 * @return \mef\Log\Entry\EntryInterface
	 */
	public function createLogEntry($level, $message, array $context = []);
}