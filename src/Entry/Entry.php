<?php namespace mef\Log\Entry;

use DateTimeInterface;

/**
 * A log entry.
 *
 * Normally there is no need to create one of these objects. It is the
 * responsibility of the Logger to create one and pass it to the Handler.
 *
 * The object should be considered immutable.
 */
class Entry implements EntryInterface
{
	use EntryToStringTrait;

	/**
	 * The time the log was created.
	 *
	 * @var \DateTimeInterface
	 */
	private $dateTime;

	/**
	 * The log level.
	 *
	 * It must be one of the Psr\Log\LogLevel constants.
	 *
	 * @var string
	 */
	private $level;

	/**
	 * The message to display.
	 *
	 * This message is in post-interpolated form. i.e., The handler will
	 * (probably) log this message as-is.
	 *
	 * @var string
	 */
	protected $message;

	/**
	 * Extra data that the handler may optional use.
	 *
	 * @var array
	 */
	protected $context;

	/**
	 * Constructor.
	 *
	 * @param \DateTimeInterface DateTimeInterface  time of log
	 * @param string             $level             a Psr\Log\LogLevel constant
	 * @param string             $message           the message to log
	 * @param array              $context           extra data
	 */
	public function __construct(DateTimeInterface $dateTime, $level, $message, array $context = [])
	{
		$this->dateTime = $dateTime;
		$this->level = (string) $level;
		$this->message = (string) $message;
		$this->context = $context;
	}

	/**
	 * Return the time of when the log entry was created.
	 *
	 * @return \DateTimeInterface
	 */
	public function getDateTime()
	{
		return $this->dateTime;
	}

	/**
	 * Return the severity level.
	 *
	 * @return string
	 */
	public function getLevel()
	{
		return $this->level;
	}

	/**
	 * Return the message.
	 *
	 * @return string
	 */
	public function getMessage()
	{
		return $this->message;
	}

	/**
	 * Return the extra context data.
	 *
	 * @return array
	 */
	public function getContext()
	{
		return $this->context;
	}
}