<?php namespace mef\Log\Entry;

/**
 * An interface that represents a log item.
 *
 * mef\Log\Entry implements this interface.
 */
interface MutableEntryInterface extends EntryInterface
{
	/**
	 * Replace the message with a new value.
	 *
	 * @param string $message
	 */
	public function setMessage($message);

	/**
	 * Add the value to the context under the given key.
	 *
	 * Can use null as the value to effectively unset that key, although
	 * there's no guarantee that the key will literally be unset.
	 *
	 * @param string $key
	 * @param mixed $value
	 */
	public function updateContext($key, $value);
}