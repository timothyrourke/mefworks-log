<?php namespace mef\Log\Processor;

use mef\Log\Entry\MutableEntryInterface;

/**
 * An empty implementation of a ProcessorInterface.
 */
class NullProcessor implements ProcessorInterface
{
	/**
	 * Does nothing.
	 *
	 * @param \mef\Log\Entry\MutableEntryInterface $entry
	 */
	public function process(MutableEntryInterface $entry)
	{
	}
}