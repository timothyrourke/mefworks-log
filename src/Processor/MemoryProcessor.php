<?php namespace mef\Log\Processor;

use mef\Log\Entry\MutableEntryInterface;

/**
 * Add memory usage details to the log entry.
 */
class MemoryProcessor implements ProcessorInterface
{
	/**
	 * Add memory usage details to the log entry.
	 *
	 * @param \mef\Log\Entry\MutableEntryInterface $entry
	 */
	public function process(MutableEntryInterface $entry)
	{
		$entry->updateContext('memory_usage', memory_get_usage());
	}
}
