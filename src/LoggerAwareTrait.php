<?php namespace mef\Log;

trait LoggerAwareTrait
{
	protected $logger;

	/**
	 * Return the current logger.
	 *
	 * @return \mef\Log\Logger
	 */
	public function getLogger()
	{
		return $this->logger;
	}

	/**
	 * Set the current logger.
	 *
	 * @param \mef\Log\Logger $logger
	 */
	public function setLogger(Logger $logger)
	{
		$this->logger = $logger;
	}
}