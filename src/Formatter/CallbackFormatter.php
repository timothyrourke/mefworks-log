<?php namespace mef\Log\Formatter;

use mef\Log\Entry\EntryInterface;

/**
 * Useful to create formatters without having to subclass or create a
 * standalone class.
 *
 * <code>
 * $formatter = new mef\Log\Formatter\Callback(
 *    function(mef\Log\EntryInterface $entry) {
 *      // return formatted $entry
 *    }
 * );
 * </code>
 *
 */
class CallbackFormatter implements FormatterInterface
{
	/**
	 * A callback that takes an EntryInterface as a parameter and returns some
	 * formatted version of it.
	 *
	 * @var callable
	 */
	private $callback;

	/**
	 * The constructor.
	 *
	 * @param callable $callback  The callback that will format the LogEntry.
	 *                            It must take a mef\Log\LogEntryInterface
	 *                            parameter and return a formatted variation.
	 */
	public function __construct(callable $callback)
	{
		$this->callback = $callback;
	}

	/**
	 * Formats the entry by passing it through to the callback.
	 *
	 * @param  EntryInterface $entry
	 *
	 * @return mixed
	 */
	public function formatLogEntry(EntryInterface $entry)
	{
		return call_user_func($this->callback, $entry);
	}
}