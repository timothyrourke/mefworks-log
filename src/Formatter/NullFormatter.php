<?php namespace mef\Log\Formatter;

use mef\Log\Entry\EntryInterface;

/**
 * Returns null.
 */
class NullFormatter implements FormatterInterface
{
	/**
	 * Return null.
	 *
	 * @param  mef\Log\EntryInterface $entry
	 *
	 * @return null
	 */
	public function formatLogEntry(EntryInterface $entry)
	{
		return null;
	}
}