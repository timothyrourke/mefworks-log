<?php namespace mef\Log\Formatter;

use mef\Log\Entry\EntryInterface;

use mef\StringInterpolation\StringInterpolatorInterface;
use mef\StringInterpolation\StringInterpolatorAwareInterface;
use mef\StringInterpolation\StringInterpolatorAwareTrait;
use mef\StringInterpolation\PlaceholderInterpolator;
use mef\Stringifier\Stringifier;

/**
 * Formats the entry as a string using a string interpolator.
 */
class StringFormatter implements FormatterInterface,
	StringInterpolatorAwareInterface
{
	use StringInterpolatorAwareTrait;

	/**
	 * The format string to use with the interpolator.
	 *
	 * @var string
	 */
	private $format;

	/**
	 * Constructor
	 *
	 * @param \mef\StringInterpolation\StringInterpolatorInterface $interpolator
	 * @param string $format
	 */
	public function __construct(StringInterpolatorInterface $interpolator,
		$format)
	{
		$this->stringInterpolator = $interpolator;
		$this->format = (string) $format;
	}

	/**
	 * Set up the string formatter with reasonable defaults.
	 *
	 * @return \mef\Log\Formatter\StringFormatter
	 */
	static public function withDefaultInterpolation()
	{
		return new StringFormatter(
			new PlaceholderInterpolator(new Stringifier),
			'{date} [{level}] {message} {context}'
		);
	}

	/**
	 * Format the $entry as a string
	 *
	 * @param  mef\Log\EntryInterface $entry
	 *
	 * @return string
	 */
	public function formatLogEntry(EntryInterface $entry)
	{
		return $this->stringInterpolator->getInterpolatedString(
			$this->format, [
				'date' => $entry->getDateTime(),
				'level' => $entry->getLevel(),
				'message' => $entry->getMessage(),
				'context' => $entry->getContext() ?: '',
			]
		);
	}
}