<?php namespace mef\Log\Formatter;

use mef\Log\Entry\EntryInterface;

/**
 * An interface for formatting a mef\Log\LogEntryInterface object.
 */
interface FormatterInterface
{
	/**
	 * Return a formatted version of $entry.
	 *
	 * The return type can be anything. Handlers must be given a formatter
	 * that returns data in a version that it is expecting. e.g., A handler
	 * that logs to a text file should only be given a FormatterInterface
	 * object that returns a string.
	 *
	 * @param  EntryInterface $entry
	 *
	 * @return mixed
	 */
	public function formatLogEntry(EntryInterface $entry);
}