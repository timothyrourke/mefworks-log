<?php namespace mef\Log;

use Psr\Log\AbstractLogger;
use Psr\Log\LogLevel;
use Psr\Log\InvalidArgumentException;

use mef\Log\Entry\EntryFactoryInterface;
use mef\Log\Entry\EntryFactoryAwareInterface;
use mef\Log\Entry\EntryFactoryAwareTrait;
use mef\Log\Handler\HandlerAwareInterface;
use mef\Log\Handler\HandlerAwareTrait;
use mef\Log\Handler\HandlerInterface;

/**
 * The Logger class is used to log data.
 *
 * It requires two objects to do something useful:
 *
 * 1) an EntryFactoryInterface that creates a log entry object based on the
 * parameters to log(), and
 *
 * 2) a HandlerInterface object that does something with that log entry
 * object.
 *
 * <code>
 * $interpolator = new PlaceholderInterpolator(new Stringifier);
*  $factory = new DeferredEntryFactory($interpolator);
 * $handler = new FileHandler('/tmp/my.log');
 * $logger = new mef\Log\Logger($factory, $handler);
 * $logger->info('Hello, {name}', ['name' => 'Matthew']);
 * </code>
 *
 * The above code sets up a DeferredEntryFactory (it defers any processing
 * until the log entry is handled) and a file based handler. The interpolator
 * is used to replace the placeholders with actual data.
 *
 * Note that you can use the StandardLogger class to avoid setting up all
 * of these objects yourself.
 */
class Logger extends AbstractLogger implements HandlerAwareInterface,
	EntryFactoryAwareInterface
{
	use FilterTrait;
	use HandlerAwareTrait;
	use EntryFactoryAwareTrait;

	/**
	 * An array that maps the Psr log levels to a severity index.
	 *
	 * @var array
	 */
	static private $levels = [
		LogLevel::EMERGENCY => 8,
		LogLevel::ALERT => 7,
		LogLevel::CRITICAL => 6,
		LogLevel::ERROR => 5,
		LogLevel::WARNING => 4,
		LogLevel::NOTICE => 3,
		LogLevel::INFO => 2,
		LogLevel::DEBUG => 1
	];

	/**
	 * Contructor.
	 *
	 * @param \mef\Log\Entry\EntryFactoryInterface $logEntryFactory
	 * @param \mef\Log\Handler\HandlerInterface $handler
	 */
	public function __construct(
		EntryFactoryInterface $logEntryFactory,
		HandlerInterface $handler
	) {
		$this->logEntryFactory = $logEntryFactory;
		$this->handler = $handler;
	}

	/**
	 * Pass a message to the log handler.
	 *
	 * If there is no attached handler or the handler does not respond to the
	 * level, nothing happens. Otherwise, the message is first sent to the
	 * processor (if it exists) which may add additional information.
	 *
	 * The message is then interpolated via data from the $context array
	 * (if an interpolator is attached). That interpolated string and
	 * remaining context data is then passed to the handler.
	 *
	 * Usually the convenience methods are used:
	 *
	 * $logger->debug($message, array $context = [])
	 * $logger->info($message, array $context = [])
	 * $logger->notice($message, array $context = [])
	 * $logger->warning($message, array $context = [])
	 * $logger->error($message, array $context = [])
	 * $logger->critical($message, array $context = [])
	 * $logger->alert($message, array $context = [])
	 * $logger->emergency($message, array $context = [])
	 *
	 * @param string $level    a valid Psr\LogLevel constant
	 * @param string $message  the string message with {placeholders}
	 * @param array $context   any extra data to be interpolated
	 */
	public function log($level, $message, array $context = [])
	{
		if (isset(self::$levels[$level]) === false)
		{
			throw new InvalidArgumentException($level);
		}

		if (isset($this->filteredLevels[$level]) === false)
		{
			$entry = $this->logEntryFactory->createLogEntry(
				$level, $message, $context);
			$this->handler->handleLogEntry($entry);
		}
	}

	/**
	 * Returns an array of error levels.
	 *
	 * The key is the name of the error level.
	 * The value is an integer that represents the severity. Larger numbers
	 * are considered to be more severe.
	 *
	 * @return array
	 */
	static public function getErrorLevels()
	{
		return self::$levels;
	}
}