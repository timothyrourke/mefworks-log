<?php namespace mef\Log;

use Psr\Log\LogLevel;

use mef\Log\Entry\DeferredEntryFactory;
use mef\Log\Formatter\StringFormatter;
use mef\Log\Handler\HandlerInterface;
use mef\Log\Handler\ChainHandler;
use mef\Log\Handler\StderrHandler;
use mef\Log\Handler\StdoutHandler;
use mef\Stringifier\Stringifier;
use mef\StringInterpolation\PlaceholderInterpolator;

/**
 * A convenience class to set up a logger that logs (by default) to
 * stdout and stderr.
 *
 * Any message less severe than warning will go to stdout. All other
 * messages go to stderr.
 *
 * A PlaceholderInterpolation object is created to use as the
 * message interpolator.
 */
class StandardLogger extends Logger
{
	/**
	 * Constructor
	 *
	 * See class documentation for implementation details.
	 *
	 * @param \mef\Log\Handler\HandlerInterface $handler
	 */
	public function __construct(HandlerInterface $handler = null)
	{
		if ($handler === null)
		{
			$formatter = StringFormatter::withDefaultInterpolation();

			$stderrHandler = new StderrHandler($formatter);
			$stderrHandler->filterLowerThan(LogLevel::WARNING);

			$stdoutHandler = new StdoutHandler($formatter);

			$handler = new ChainHandler;
			$handler->pushHandler($stderrHandler, true);
			$handler->pushHandler($stdoutHandler);
		}

		$interpolator = new PlaceholderInterpolator(new Stringifier);
		$factory = new DeferredEntryFactory($interpolator);

		parent::__construct($factory, $handler);
	}
}