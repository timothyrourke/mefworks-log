<?php namespace mef\Log;

use Psr\Log\LogLevel;

use mef\Log\Logger;

/**
 * Add filtering to a handler.
 *
 * Almost every standard Handler implements this trait.
 *
 * The most common use is to filter out message lower than some threshold.
 * e.g., You might want to log some events to a file, but other more important
 * ones get emailed to you.
 *
 * <code>
 * $handler = new mef\Log\Handler\Stdout;
 * $handler->filterLowerThan(Psr\Log\LogLevel::WARNING);
 *
 * $logger = new Logger($handler);
 * $logger->info("He's wearing a red shirt."); // will be ignored
 * $logger->warning("He's dead, Jim.");        // will be seen in stdout
 * </code>
 *
 * Note that there is a difference in performance between filtering out entries
 * in a handler and in the base Logger instance. If an attempt is made to log
 * something at a level that the Logger itself does not care about, then no
 * processing is done at all. However, if the Logger is set up to handle the
 * entry and there is a handler attached, it will always process the event,
 * even if the handler itself ignores it.
 */
trait FilterTrait
{
	/**
	 * An array of filtered levels.
	 *
	 * The key is the log level; if set, the level is not handled.
	 *
	 * @var array
	 */
	private $filteredLevels = [];

	/**
	 * Filter out any log entry of exactly $level severity.
	 *
	 * @param  string $level  a Psr\Log\LogLevel constant
	 */
	public function filter($level)
	{
		$this->filteredLevels[$level] = true;
	}

	/**
	 * Filter out all levels. (Nothing will be logged.)
	 */
	public function filterAll()
	{
		$this->filteredLevels = Logger::getErrorLevels();
	}

	/**
	 * Filter out all log entries except of the specified level.
	 *
	 * (Only that level will be logged.)
	 *
	 * @param  string $level  a Psr\Log\LogLevel constant
	 */
	public function filterAllExcept($level)
	{
		$this->filterAll();
		unset($this->filteredLevels[$level]);
	}

	/**
	 * Filter out any log entry that is lower than $level severity.
	 *
	 * @param  string $level  a Psr\Log\LogLevel constant
	 */
	public function filterLowerThan($level)
	{
		$this->updateFilterLevels($level, true, function($a, $b) {
			return $a < $b;
		});
	}

	/**
	 * Filter out any log entry that is higher than $level severity.
	 *
	 * @param  string $level  a Psr\Log\LogLevel constant
	 */
	public function filterHigherThan($level)
	{
		$this->updateFilterLevels($level, true, function($a, $b) {
			return $a > $b;
		});
	}

	/**
	 * Permit any log entry of exactly $level severity.
	 *
	 * @param  string $level  a Psr\Log\LogLevel constant
	 */
	public function unfilter($level)
	{
		unset($this->filteredLevels[$level]);
	}

	/**
	 * Permit any log entry lower than $level severity.
	 *
	 * @param  string $level  a Psr\Log\LogLevel constant
	 */
	public function unfilterLowerThan($level)
	{
		$this->updateFilterLevels($level, null, function($a, $b) {
			return $a < $b;
		});
	}

	/**
	 * Permit any log entry higher than $level severity.
	 *
	 * @param  string $level  a Psr\Log\LogLevel constant
	 */
	public function unfilterHigherThan($level)
	{
		$this->updateFilterLevels($level, null, function($a, $b) {
			return $a > $b;
		});
	}

	/**
	 * Return true if the handler wants to handle this level of severity.
	 *
	 * @param  string $logLevel   a Psr\Log\LogLevel constant
	 *
	 * @return boolean
	 */
	public function willHandleLevel($logLevel)
	{
		return isset($this->filteredLevels[$logLevel]) === false;
	}

	/**
	 * Updates the filter levels.
	 *
	 * @param  string   $level   a Psr\Log\LogLevel constant
	 * @param  mixed    $value   the value to set, if the callback returns true
	 * @param  callable $cb      a callable function taking two integer
	 *                           arguments, the first being the tested level,
	 *                           and the second representing the passed $level
	 */
	private function updateFilterLevels($level, $value, callable $cb)
	{
		$levels = Logger::getErrorLevels();

		if (isset($levels[$level]) === true)
		{
			foreach ($levels as $levelName => $levelSeverity)
			{
				if ($cb($levelSeverity, $levels[$level]) === true)
				{
					$this->filteredLevels[$levelName] = $value;
				}
			}
		}
	}
}