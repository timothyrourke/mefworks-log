<?php namespace mef\Log\Example;

require_once __DIR__ . '/../vendor/autoload.php';

// Custom handlers need to implmement the mef\Log\Handler\HandlerInterface,
// which defines two methods: handleLogEntry and handleLogEntryBatch. In most
// cases, extending the AbstractHandler is appropriate.
//
// It implements a default batch method that calls the handleLogEntry for each
// entry. It also uses the mef\Log\FilterTrait, which adds the capability to
// filter out events by log level.
//
// This handler simply associates the logged entry with the $lastEntry instance
// variable.
//
class MyHandler extends \mef\Log\Handler\AbstractHandler
{
	public $lastEntry;

	public function handleLogEntry(\mef\Log\Entry\EntryInterface $entry)
	{
		$this->lastEntry = $entry;
	}
}

$myHandler = new MyHandler;

$logger = new \mef\Log\StandardLogger($myHandler);
$logger->debug('You won\'t see this message.');
$logger->debug('Hello, World!');

echo $myHandler->lastEntry->getMessage(), PHP_EOL;