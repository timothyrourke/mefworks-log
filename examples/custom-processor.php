<?php namespace mef\Log\Example;

require_once __DIR__ . '/../vendor/autoload.php';

// Custom handlers need to implmement the mef\Log\Handler\HandlerInterface,
// which defines two methods: handleLogEntry and handleLogEntryBatch. In most
// cases, extending the AbstractHandler is appropriate.
//
// It implements a default batch method that calls the handleLogEntry for each
// entry. It also uses the mef\Log\FilterTrait, which adds the capability to
// filter out events by log level.
//
// This handler simply associates the logged entry with the $lastEntry instance
// variable.
//
class MyProcessor implements \mef\Log\Processor\ProcessorInterface
{
	public function process(\mef\Log\Entry\MutableEntryInterface $entry)
	{
		// A mutable entry can have its message modified
		$entry->setMessage(strtoupper($entry->getMessage()));

		// It can also have its context updated ... this will set the key
		// 'processed' to the value true.
		$entry->updateContext('processed', true);

		// The log level and date cannot be changed.
	}
}

$myProcessor = new MyProcessor;

$logger = new \mef\Log\StandardLogger;
$logger->getLogEntryFactory()->setProcessor($myProcessor);
$logger->info('Hello, World!');

// The above message will be processed before it shows up on stdout ... so
// the message be displayed in upper case letters.